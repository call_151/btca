//
//  MyUserDefault.swift
//  fbCollectionView
//
//  Created by call151 on 2018-11-23.
//  Copyright © 2018 call151. All rights reserved.
//

import Foundation


var nameOfDevice = "nameOfDevice"


class MyUserDefault {
    
    var lastUSedCycleAnalystDevice = ""
    var myCycleAnalystDevice: CycleAnalystDevice?
    var debug = true
    
    
    
    //*********************************************************************************
    //     last used device and userdefautl
    //
    func giveMeTheNameOfLastUSedCycleAnalystDevice() -> String? {
        let defaults = UserDefaults.standard
        if let lastUSedCycleAnalystDevice = defaults.string(forKey: nameOfDevice) {
            return lastUSedCycleAnalystDevice
        }else {
            return nil
        }
    }
    
    func saveTheNameOfLastUSedCycleAnalystDevice(theNameOfDevice: String) {
        let defaults = UserDefaults.standard
        defaults.set(theNameOfDevice, forKey: nameOfDevice)
    }
    
    
    //*********************************************************************************
    //     device configuration in/out from file 
    //
    func openCycleAnalystDeviceConfigurationFromFile(named fileName: String) -> CycleAnalystDevice?
    {
        if let url = try? FileManager.default.url(
            for: .documentDirectory,
            in: .userDomainMask,
            appropriateFor: nil,
            create: true
            ).appendingPathComponent(fileName)
            {
                if let jsonData = try? Data(contentsOf: url)
                {
                    if let newValue = try? JSONDecoder().decode(CycleAnalystDevice.self, from: jsonData){
                        myCycleAnalystDevice = newValue
                        if debug { print ("Reading CADevice from file -> Success") }
                    }else{
                        myCycleAnalystDevice = nil
                        if debug { print ("Reading CADevice from file -> FAILED") }
                    }
                }
                else {
                    if debug { print ("Reading CADevice from file -> fileName does not exist at ppath") }
                    myCycleAnalystDevice = nil
                }
            }
        return myCycleAnalystDevice
    }
    
    
    func openCycleAnalystDeviceConfigurationFromFile2(named fileName: String) -> CycleAnalystDevice?
    {
        if let url = try? FileManager.default.url(
            for: .documentDirectory,
            in: .userDomainMask,
            appropriateFor: nil,
            create: true
            ).appendingPathComponent(fileName)
        {
  
            if let jsonData = try? Data(contentsOf: url)
            {
                if let newValue = CycleAnalystDevice.create(json: jsonData) {
                    myCycleAnalystDevice = newValue
                    if debug { print ("Reading CADevice from file -> Success") }
                }else{
                    myCycleAnalystDevice = nil
                    if debug { print ("Reading CADevice from file -> FAILED") }
                }
            }
            else {
                if debug { print ("Reading CADevice from file -> fileName does not exist at ppath") }
                myCycleAnalystDevice = nil
            }
        }
        return myCycleAnalystDevice
    }
    
    

    
    
    func saveCycleAnalystDeviceConfigurationInFile(named fileName: String)
    {
        if let json = myCycleAnalystDevice?.json
        {
            if let url = try? FileManager.default.url(
                for: .documentDirectory,
                in: .userDomainMask,
                appropriateFor: nil,
                create: true
                ).appendingPathComponent(fileName)
                {
                    do {
                        try  json.write(to: url)
                         if debug { print ("Writing CADevice to file -> Success") }
                    } catch let error{      //TODO: todo: fraco y a til encore erreur dans ca
                         if debug  {
                            print ("Writing CADevice to file -> FAILED with Error \(error)")
                            
                        }
                    }
                }
        }
    }
    
    
    

    
}
