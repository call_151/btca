//
//  BluetoothManager.swift
//  BTCA
//
//  Created by call151 on 2018-11-27.
//  Copyright © 2018 call151. All rights reserved.
//

import UIKit
import CoreBluetooth

let HOW_MANY_SCAN = 2                           // 2 = 1 scan :-) sorry
let COUNT_DOWN_TIMER_DATA: Double = 10          // Scanning time
let TIMER_TO_SEND_FAKE_DATA: Double = 2


extension Notification.Name {
    static let BTManagerArrayDidChange = Notification.Name("BTManagerArrayDidChange")
}

class BluetoothManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate  {

    // MARK: - CoreBlueTooth
    //_______________________________________________________________________________________________________
    //--
    //--
    //--                CoreBlueTooth
    //--
    //_______________________________________________________________________________________________________
    let debug = true
    let bidonBool = true // TODO: todo remove 
    
    weak var delegate: BluetoothManagerDelegate?
    weak var delegateForPeripheral: BluetoothManagerPeripheralDidConnectDelegate?
    
    
    var firstTime = true
    var txCharacteristic : CBCharacteristic?
    var rxCharacteristic : CBCharacteristic?
    var characteristicASCIIValue = NSString()
    var oldAscii = ""
    var newAscii = ""
    
    var myCentralManager: CBCentralManager!
    var peripheralsDiscovered = [CBPeripheral]()
    var peripheralsRSSI = [NSNumber]()
    var myBTPeripheral: CBPeripheral!
    //var peripheralsRSSIold = [UUID: NSNumber]()                           // not sure i need it, i used to be sure the device is not already in the lsit
    
    //temporair e
    var lesServicesEtCharacteristic = ""
    
    
    var howManyScan = HOW_MANY_SCAN                                         // number of time, we do the scan/no scan. before stopping for good
    
    var theScanningTimer = Timer()                                          // The timer we user to scan for a time, then stop for the same time then start again
    let theScanningTimerInterval: Double = COUNT_DOWN_TIMER_DATA            // we scan 10 seconds, then stop 10
    
    var countDownTimer = Timer()                                            // the timer we use to write the countdonn in the UIBarBUttonItem
    var countDownTimerData = COUNT_DOWN_TIMER_DATA
    
    
    var timerBeforeSendingfakeDataToUART = Timer()
    var timerBeforeSendingfakeDataToUARTDATA = TIMER_TO_SEND_FAKE_DATA

    
    var bluetootOn = false                                                  // if bluetooth off, scaning is impossible
    var bluetoothScanning = false                                           // to keep track of scanning toggle

    //TODO: - TODO franco arrange ca
    var showUARTDeviceOnly = true
    
    var scanningState = "" {
        didSet {
            delegate?.BluetoothManagerLabelChanged(self)
        }
    }
    
    
    override init(){
        super.init()
        myCentralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        
        // the CBCentralManagerOptionShowPowerAlertKey will show an alert if user has bluetooth off
        // strange, with bluetooth off and the option i get
    }
    
    
    func myStartScan(){
        theScanningTimer.invalidate()
        countDownTimer.invalidate()
        
        if debug {print ("StartScan ")}
        
        if myCentralManager.state == .poweredOn {
            bluetoothScanning = true
            startTheTimer()
            scanningState = "Scanning"
            scanBLEWithOption(showUARTDeviceOnly)
        }
        
    }
    
    func fullStopScaning(){
        theScanningTimer.invalidate()
        countDownTimer.invalidate()
        scanningState = "Not Scanning"
        myCentralManager.stopScan()
    }
    
    func myStopScan(){
        theScanningTimer.invalidate()
        countDownTimer.invalidate()
        
        if debug {print ("StopScan - Counting Down" )}
        
        if myCentralManager.state == .poweredOn {
            bluetoothScanning = false
            startCountDown()
            scanningState = "Not Scanning"
            myCentralManager.stopScan()
            
        }
    }
    
    func myContinuScan(){
        howManyScan = HOW_MANY_SCAN
        if debug { print("ok je continu scan avec \(howManyScan) left") }
        myStartScan()
    }
    
    
    func emptyListAndStartNewScan(){
        howManyScan = HOW_MANY_SCAN
        peripheralsDiscovered = [CBPeripheral]()
        peripheralsRSSI = [NSNumber]()
        delegate?.BluetoothManagerNewDeviceFound(self)
        myStartScan()
    }
    
    
    
    func scanBLEWithOption (_ withUARTService: Bool){
        howManyScan = howManyScan - 1
        if debug {print ("howManyScan = \(howManyScan) ")}
        
        if howManyScan > 0 {
            if withUARTService {
                // search for bluetooth device that support UART service
                myCentralManager.scanForPeripherals(withServices: [BLEService_UUID] , options: [CBCentralManagerScanOptionAllowDuplicatesKey:false])
                if debug {print ("With service Option" )}
                
            }else {
                // search for all bleutooth device
                myCentralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey:false])
                if debug {print ("Without option" )}
            }
            
        }else {
            if debug {print ("je suis dans <= 0  pourquoi ca arrete pas ")}
            theScanningTimer.invalidate()
            countDownTimer.invalidate()
            myCentralManager.stopScan()
            scanningState = "scan off"
            bluetoothScanning = false
        }
        
    }
    
    
    
    func startTheTimer(){
        theScanningTimer.invalidate()
        theScanningTimer = Timer.scheduledTimer(timeInterval: theScanningTimerInterval, target: self, selector: #selector(toggleBTScan), userInfo: nil, repeats: true)
    }
    
    func stopTheTimer(){
        if debug {print ("legacy 1 ")}
        theScanningTimer.invalidate()
    }
    
    
    func startCountDown() {
        countDownTimerData = theScanningTimerInterval
        countDownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCountDown), userInfo: nil, repeats: true)
    }
    
    @objc func updateCountDown(){
        countDownTimerData =  countDownTimerData - 1
        let counterStep = Int (countDownTimerData)
        scanningState = ("Scanning in \(counterStep)")
        
        if countDownTimerData <= 0 {
            //countDownTimer.invalidate()
            myStartScan()
        }
    }
    
    
    @objc func toggleBTScan(){
        if debug {print ("ToggleBTScan" )}
        
        if bluetootOn {
            if bluetoothScanning{
                myStopScan()
            }else{
                myStartScan()
            }
        }
    }
    
    
    func startFaketimer(){
        timerBeforeSendingfakeDataToUART = Timer.scheduledTimer(timeInterval: timerBeforeSendingfakeDataToUARTDATA, target: self, selector: #selector(outgoingData), userInfo: nil, repeats: true)
    }
 
    
    // MARK: - CBCentralManagerDelegate
    //_______________________________________________________________________________________________________
    //--
    //--
    //--                CBCentralManagerDelegate
    //--
    //_______________________________________________________________________________________________________
    
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch (central.state) {
        case .poweredOff:
            if debug  {print ("CBCentralManager - Case poweredOff")}
            bluetootOn = false
            myStopScan()
            
        case .poweredOn:
            if debug  {print ("CBCentralManager - Case poweredOn")}
            bluetootOn = true
            myStartScan()
            
        case .unknown:
            if debug  {print ("CBCentralManager - Case unknown")}
            
        case .resetting:
            if debug  {print ("CBCentralManager - Case resetting")}
            
        case .unsupported:
            if debug  {print ("CBCentralManager - Case unsupported")}
            
        case .unauthorized:
            if debug  {print ("CBCentralManager - Case unauthorized")}
        }
    }
    
    
    //    Discovering and Retrieving Peripherals
    //    - centralManager:didDiscoverPeripheral:advertisementData:RSSI:
    //          Invoked when the central manager discovers a peripheral while scanning.
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        // old way
        
        //     if debug {print ("device found ")}
        let newUUID = peripheral.identifier
        var found = false
        for unPeripheral in peripheralsDiscovered{
            let theUUID = unPeripheral.identifier
            if theUUID == newUUID{
                found=true
                break
            }
        }
        if !found{
            peripheralsDiscovered.append(peripheral)
            peripheralsRSSI.append(RSSI)
            delegate?.BluetoothManagerNewDeviceFound(self)
            delegate?.BluetoothManagerNewDeviceFound2(self, device: peripheral )
        
        }
        
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        peripheral.discoverServices([BLEService_UUID])
        myBTPeripheral = peripheral
        delegateForPeripheral?.BluetoothManagerPeripheralDidConnect(self, device: peripheral)
        startFaketimer()
    }
    
    //_______________________________________________________________________________________________________
    //--
    //--
    //--                CBPeripheralDelegate
    //--
    //_______________________________________________________________________________________________________
    
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?){
        if debug { print ("franco didDiscoverServices") }
        if let lesServices = peripheral.services
        {
            for aService in lesServices {
                let aServiceUUID = aService.uuid
                if aServiceUUID == BLEService_UUID{
                    // test
                }
                peripheral.discoverCharacteristics(nil, for: aService)
            }
        }
    }
    
    
    
    // temporaire le temps que je verifie
    
    
    
//    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
//
//        if debug{
//            print ("in didDiscover Characteristic ")
//            print ("Service         = \(service.uuid)")
//            print ("BLEService_UUID = \(BLEService_UUID)")
//        }
//
//        //        if let lesCharacteristics = service.characteristics
//        //        {
//        //            for uneCharacteristique in lesCharacteristics {
//        //               // let uneCharacteristiqueUUID = uneCharacteristique.uuid
//        //            }
//        //        }
//    }
    
    // temporaire to test
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if debug{
            //            print ("in didDiscover Characteristic ")
            //            print ("Service         = \(service.uuid)")
            //            print ("BLEService_UUID = \(BLEService_UUID)")
            //        }
        if let lesCharacteristics = service.characteristics
        {
            for uneCharacteristique in lesCharacteristics {
                print ("Detail - Une Characteristique \(uneCharacteristique)")
                lesServicesEtCharacteristic = lesServicesEtCharacteristic +  "\t\tDetail - Une Characteristique Descriptors \(uneCharacteristique.descriptors?.description ?? "No Descriptor")" + "\n"
                //        lesServicesEtCharacteristic = lesServicesEtCharacteristic +  "\t\tDetail - Une Characteristique Propreties \(uneCharacteristique.properties.description ?? "No Propreties")" + "\n"
                lesServicesEtCharacteristic = lesServicesEtCharacteristic +  "\t\tDetail - Une Characteristique Values \(uneCharacteristique.value?.description ?? "No Value")" + "\n"
                
                
                
                //ServiceAndCharacteristicTextView.text = lesServicesEtCharacteristic
                print ("franco \(lesServicesEtCharacteristic ) ")
                
                if uneCharacteristique.uuid.isEqual(BLE_Characteristic_uuid_Rx)  {
                    rxCharacteristic = uneCharacteristique
                    
                    //Once found, subscribe to the this particular characteristic...
                    peripheral.setNotifyValue(true, for: rxCharacteristic!)
                    self.updateIncomingData ()
                    // We can return after calling CBPeripheral.setNotifyValue because CBPeripheralDelegate's
                    // didUpdateNotificationStateForCharacteristic method will be called automatically
                    peripheral.readValue(for: uneCharacteristique)
                    print("Rx Characteristic: \(uneCharacteristique.uuid)")
                }
                if uneCharacteristique.uuid.isEqual(BLE_Characteristic_uuid_Tx){
                    txCharacteristic = uneCharacteristique
                    print("Tx Characteristic: \(uneCharacteristique.uuid)")
                }
            }
        }
    }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print("*******************************************************")
        
        if (error != nil) {
            print("Error changing notification state:\(String(describing: error?.localizedDescription))")
            
        } else {
            print("Characteristic's value subscribed")
        }
        
        if (characteristic.isNotifying) {
            print ("Subscribed. Notification has begun for: \(characteristic.uuid)")
        }
    }
    

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if characteristic == rxCharacteristic {
            if let ASCIIstring = NSString(data: characteristic.value!, encoding: String.Encoding.utf8.rawValue) {
                self.characteristicASCIIValue = ASCIIstring
                print("Value Recieved: \((characteristicASCIIValue as String))")
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "Notify"), object: nil)
                
            }
        }
    }
    
    func updateIncomingData () {
//        if firstTime{
//            firstTime = false
//            self.outgoingData ()
//        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "Notify"), object: nil , queue: nil){
            notification in
            //let appendString = "\n"
//            let appendString = ""
//            let myFont = UIFont(name: "Helvetica Neue", size: 15.0)
            //            let myAttributes2 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): myFont!, convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.red]
            //            let attribString = NSAttributedString(string: "[Incoming]: " + (characteristicASCIIValue as String) + appendString, attributes: convertToOptionalNSAttributedStringKeyDictionary(myAttributes2))
            //
            //            let attribString = NSAttributedString(string: "" + (characteristicASCIIValue as String) + appendString, attributes: convertToOptionalNSAttributedStringKeyDictionary(myAttributes2))
            //
            //
            
            //print(characteristicASCIIValue)
            self.newAscii = self.characteristicASCIIValue as String
            self.oldAscii.append(self.newAscii)
            //            print ("Avant le split j'ai \(self.oldAscii)")
            // print(Array(self.oldAscii.unicodeScalars))
            //            print("le code ASCII ?")
            //          print(Array(self.newAscii.unicodeScalars))
            
            //             U+000A   ca serait le \n en unicode
            
            //let laLettre = "\n"
            //            let laLettre = "\u{1F1FA}"
            
            
            if let jaiTrouve = self.oldAscii.firstIndex(of: "\r") {
                //print ("J'ai trouvé \(jaiTrouve)")
                print ("oldAscii AVANT -> \(self.oldAscii)")
                let reponse = self.oldAscii.prefix(upTo: jaiTrouve)
                //                print ("La Ligne est \(reponse)")
                print(Array(reponse.unicodeScalars))
                print ("oldAscii APRES 1 -> \(self.oldAscii)")
                
                
//                let monStartIndex = self.oldAscii.startIndex
//                let MonEndIndex =    jaiTrouve
                
                
                
                
                //                var str = "Hello, playground"
                //                str.substringWithRange(Range<String.Index>(start: str.startIndex.advancedBy(2), end: str.endIndex.advancedBy(-1))) //"llo, playgroun"
                
                //                self.oldAscii.substringWith(Range<String.Index>(monStartIndex, in: MonEndIndex))
                //self.oldAscii.removeSubrange(myRange)
                print ("oldAscii APRES 2 -> \(self.oldAscii)")
                
                print ("La Ligne est \(reponse)")
                
            }
            
            //            else{
            //                print ("Non J'ai pas trouvé ")
            //            }
            
//
//            let newAsciiText = NSMutableAttributedString(attributedString: self.consoleAsciiText!)
//            self.baseTextView.attributedText = NSAttributedString(string: characteristicASCIIValue as String , attributes: convertToOptionalNSAttributedStringKeyDictionary(myAttributes2))
//
//            newAsciiText.append(attribString)
//            newAsciiText.append(attribString)
//            self.consoleAsciiText = newAsciiText
//            self.baseTextView.attributedText = self.consoleAsciiText
            
        }
    }
    
    
    func checkState(aPeripheral: CBPeripheral){
        var stateText = ""
        if debug {print ("in checkState")}
        switch (aPeripheral.state) {
        case .disconnected:
            stateText = "Peripheral - disconnected"
        case .connecting:
            stateText = "Peripheral - connecting"
        case .connected:
            stateText = "Peripheral - connected"
            BTPeripheral.discoverServices(nil)
        case .disconnecting:
            stateText = "Peripheral - disconnecting"
        }
        if debug {print ("stateText \(stateText)")}
        //deviceStateLabel.text = "State: " + stateText
    }
    
    
    //__________________________________________________________________________________________________________________________________________
    
    
    
    //__________________________________________________________________________________________________________________________________________
    //  detail view remis ici
    //_______________________________________________________________________________________________________
    //--
    //--
    //--                UIViewController
    //--
    //_______________________________________________________________________________________________________
    
    // delegate = self
    
    //    override func viewDidLoad() {
    //        super.viewDidLoad()
    //
    //        stateText = ""
    //        deviceNameLabel.text = ""
    //        lesServicesEtCharacteristic = ""
    //        uartString = "UART ?"
    //        UARTCapableLabel.text = uartString
    //        if let _ = theBigBTManager{
    //            theBigBTManager.delegateForPeripheral = self
    //        }
    //
    //    }
    //
    
    //    override func viewDidAppear(_ animated: Bool) {
    //        configureView()
    //        if let _ = BTPeripheral{
    //            checkState(aPeripheral: BTPeripheral)
    //        }
    //    }
    //
    
    
    
    
    
    func configureView(){
        if BTPeripheral != nil {
            BTPeripheral.delegate = self
            //let leNom = ( BTPeripheral.name ??  "No Name" )
        }
    }
    
    
    
    
    
    
    var theBigBTManager: BluetoothManager!
    var BTPeripheral: CBPeripheral!
    
    var isUARTCapableServiceCBUUID = CBUUID()
    
    
    func BigBTManagerPeripheralDidConnect(_ sender: BluetoothManager) {
        if let aPeripheral = BTPeripheral {
            checkState(aPeripheral: aPeripheral)
        }
    }
    
    
    @objc
    func reCheckDevice(){
        if debug {print ("Ouais j'ai du passer ici ")}
        checkState(aPeripheral: BTPeripheral)
    }
    
    
    
    
    
    
    
    
    
    
    @objc
    func outgoingData() {
    
        let inputText = "franco send data \n"
        
//        let myFont = UIFont(name: "Helvetica Neue", size: 15.0)
//        let myAttributes1 = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): myFont!, convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.blue]
        
        writeValue(data: inputText)
        
//        let attribString = NSAttributedString(string: "[Outgoing]: " + inputText! + appendString, attributes: convertToOptionalNSAttributedStringKeyDictionary(myAttributes1))
//        let newAsciiText = NSMutableAttributedString(attributedString: self.consoleAsciiText!)
        newAscii.append(inputText)
        
//        consoleAsciiText = newAsciiText
//        baseTextView.attributedText = consoleAsciiText
//        //erase what's in the text field
//        inputTextField.text = ""
        
    }
    
    // Write functions
    func writeValue(data: String){
        let valueString = (data as NSString).data(using: String.Encoding.utf8.rawValue)
        //change the "data" to valueString
  
            if let txCharacteristic = txCharacteristic {
                self.myBTPeripheral.writeValue(valueString!, for: txCharacteristic, type: CBCharacteristicWriteType.withResponse)
            }

    }
    
    func writeCharacteristic(val: Int8){
        var val = val
        let ns = NSData(bytes: &val, length: MemoryLayout<Int8>.size)
        self.myBTPeripheral.writeValue(ns as Data, for: txCharacteristic!, type: CBCharacteristicWriteType.withResponse)
    }
    

}





// MARK: - My Protocol
//_______________________________________________________________________________________________________
//--
//--
//--                How to create the whole Delegate thing
//--
//_______________________________________________________________________________________________________

// PROTOCOL !
//_______________________________________________________________________________________________________
// In the file who want to Delegate stuff

// Before or After the class implantation
// in a protocol car we have var, i think so, to check
// Maybe I need to Check if Static is needed
//step 1
protocol BluetoothManagerDelegate: class {
    func BluetoothManagerNewDeviceFound (_ sender : BluetoothManager )
    func BluetoothManagerNewDeviceFound2 (_ sender : BluetoothManager, device: CBPeripheral )
    func BluetoothManagerLabelChanged(_ sender : BluetoothManager )
}




// PROTOCOL 2
//_______________________________________________________________________________________________________
// In the file who want to Delegate stuff

// Before or After the class implantation
// in a protocol car we have var, i think so, to check
// Maybe I need to Check if Static is needed
//step 1
protocol BluetoothManagerPeripheralDidConnectDelegate: class {
    func BluetoothManagerPeripheralDidConnect (_ sender : BluetoothManager, device:CBPeripheral )
}






